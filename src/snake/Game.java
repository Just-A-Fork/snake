package snake;

/*
	Authors: Sebastian Cypert
	FileName: Game.java
	Description:
		The most important part of the project
		it defines almost all of the logic for the snake game and
		is the starting point for it
 */

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import snake.classes.Food;
import snake.classes.GridObject;
import snake.classes.SnakeSegment;

public class Game extends Application {

	//canvas vars
	private Canvas canvas = new Canvas(800,450);
	private GraphicsContext gc_canvas = canvas.getGraphicsContext2D();
	private Canvas grid = new Canvas(800, 450);
	private GraphicsContext gc_grid = grid.getGraphicsContext2D();

	//window vars
	private StackPane root = new StackPane(canvas, grid);
	private Scene scene = new Scene(root);

	//fxml vars
	@FXML private StackPane GameOver, Main;

	//grid constants
	public final int CELL_ROWS = 70;
	public final double CELL_SIZE = grid.getWidth()/CELL_ROWS;
	public final int CELL_COLUMNS = (int)Math.floor(grid.getHeight()/CELL_SIZE);

	//game vars
	private final int[] FoodPos = new int[2];
	private int[] SnekPos;
	private int[] SnekDirection;
	private boolean Moved = true;
	private GridObject[][] GameMap;
	private final float SNAKE_SPEED = 0.1f;

	//Misc.
	private Runnable SnakeMoveFunc = () ->{

		while(true) {

			//wait between snake moves
			try {
				Thread.sleep((long)(SNAKE_SPEED * 1000));
			} catch (InterruptedException ignored) {}

			//vars
			boolean NeedGrow = false;
			int[] newSnekPos = new int[]{
					SnekPos[0]+=SnekDirection[0],
					SnekPos[1]+=SnekDirection[1]
			};
			Moved = true;

			//mod snek pos
			newSnekPos[0]%=CELL_ROWS;
			newSnekPos[1]%=CELL_COLUMNS;
			if(newSnekPos[0] < 0) newSnekPos[0]+=CELL_ROWS;
			if(newSnekPos[1] < 0) newSnekPos[1]+=CELL_COLUMNS;

			//check what we're running in to if anything
			if(getAtPos(newSnekPos[0], newSnekPos[1]) instanceof Food){
				NeedGrow = true;
				new Food(this);
			}else if(getAtPos(newSnekPos[0], newSnekPos[1]) instanceof SnakeSegment){
				try{Thread.sleep(100);}catch(InterruptedException ignore){}
				GameOver.setVisible(true);
				break;
			}

			//make segment
			SnakeSegment newSegment = new SnakeSegment(newSnekPos[0], newSnekPos[1], this);

			//kill oldest segment or grow
			if(!NeedGrow) newSegment.KillOldest();
			else NeedGrow ^= true;
		}
	};

	@Override
	public void start(Stage stage) {

		//setup scene
		scene.setFill(Color.rgb(28, 37, 51));
		scene.setOnKeyPressed(this::onKeyPress);
		FXMLLoader fxml = new FXMLLoader(getClass().getClassLoader().getResource("Menus.fxml"));
		fxml.setController(this);
		fxml.setRoot(root);
		try{fxml.load();}
		catch(Exception e){
			e.printStackTrace();
			StartGame();
		}

		//make grid
		gc_grid.setStroke(Color.rgb(48, 64, 89));
		gc_grid.setLineWidth( 2);
		for(float i = 0; i <= grid.getWidth(); i+=CELL_SIZE){
			gc_grid.strokeLine(i, 0, i, CELL_SIZE*CELL_COLUMNS);
		}
		for(float i = 0; i <= grid.getHeight(); i+=CELL_SIZE){
			gc_grid.strokeLine(0, i, CELL_SIZE*CELL_ROWS, i);
		}

		Main.setVisible(true);

		//setup window and scene
		stage.setOnCloseRequest(windowEvent -> System.exit(0));
		stage.setResizable(true);
		stage.setScene(scene);
		stage.setTitle("Snake");
		stage.show();
	}

	public void setCell(int x, int y, GridObject object){
		x%=CELL_ROWS;
		y%=CELL_COLUMNS;

		if(x < 0) x+=CELL_ROWS;
		if(y < 0) y+=CELL_COLUMNS;

		Paint tmp = gc_canvas.getFill();

		gc_canvas.setFill(object.getFill());
		gc_canvas.fillRect(x*CELL_SIZE, y*CELL_SIZE, CELL_SIZE, CELL_SIZE);
		GameMap[x][y] = object;

		gc_canvas.setFill(tmp);
	}

	public void clearCell(int x, int y){
		x%=CELL_ROWS;
		y%=CELL_COLUMNS;

		if(x < 0) x+= CELL_ROWS;
		if(y < 0) y+= CELL_COLUMNS;

		gc_canvas.clearRect(x*CELL_SIZE, y*CELL_SIZE, CELL_SIZE, CELL_SIZE);
		GameMap[x][y] = null;
	}

	public GridObject getAtPos(int x, int y){
		x%=CELL_ROWS;
		y%=CELL_COLUMNS;

		if(x < 0) x+= CELL_ROWS;
		if(y < 0) y+= CELL_COLUMNS;

		return GameMap[x][y];
	}

	@FXML
	private void onKeyPress(KeyEvent e){
		if(Moved) {
			if (e.getCode().equals(KeyCode.W) && SnekDirection[1] != 1) {
				SnekDirection[0] = 0;
				SnekDirection[1] = -1;
				Moved = false;

			} else if (e.getCode().equals(KeyCode.A) && SnekDirection[0] != 1) {
				SnekDirection[0] = -1;
				SnekDirection[1] = 0;
				Moved = false;

			} else if (e.getCode().equals(KeyCode.S) && SnekDirection[1] != -1) {
				SnekDirection[0] = 0;
				SnekDirection[1] = 1;
				Moved = false;

			} else if (e.getCode().equals(KeyCode.D) && SnekDirection[0] != -1) {
				SnekDirection[0] = 1;
				SnekDirection[1] = 0;
				Moved = false;
			}
		}
	}

	@FXML
	private void StartGame(){

		//reset vars
		Main.setVisible(false);
		GameOver.setVisible(false);
		gc_canvas.clearRect(0, 0, canvas.getWidth(), canvas.getWidth());
		GameMap = new GridObject[CELL_ROWS][CELL_COLUMNS];
		SnekPos = new int[]{ (int)Math.floor(CELL_ROWS/2), (int)Math.floor(CELL_COLUMNS/2) };
		SnekDirection = new int[]{-1, 0};
		Moved = true;

		//run startup functions
		new Food(this);
		new SnakeSegment(SnekPos[0]+1, SnekPos[1], this);
		try{Thread.sleep(50);}catch(InterruptedException ignore){}
		new SnakeSegment(SnekPos[0], SnekPos[1], this);
		new Thread(SnakeMoveFunc).start();
	}

	public static void main(String args[]){
		launch(args);
	}
}
