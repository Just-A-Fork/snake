package snake.classes;

/*
	Author: Sebastian Cypert
	FileName: SnakeSegment.java
	Description:
		the Snake Segment class defines specialised members needed to
		make the snake look like is moving
 */

import javafx.scene.paint.Color;
import snake.Game;

public class SnakeSegment extends GridObject {

	public final long BirthTime = System.currentTimeMillis();

	public SnakeSegment(int x, int y, Game gameObj){
		super(x, y, gameObj, Color.WHITE);
	}

	public void KillOldest(){

		SnakeSegment Oldest = this;

		for(GridObject i :getNeighbors()){
			if(i instanceof SnakeSegment && ((SnakeSegment) i).BirthTime<Oldest.BirthTime ){
				Oldest = (SnakeSegment)i;
			}
		}

		if(Oldest == this) Kill();
		else Oldest.KillOldest();

	}

}
