package snake.classes;
/*
	Authors: Sebastian Cypert
	FileName: Food.java
	Description:
		Defines specialized members used for spawning food
*/

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import snake.Game;

import java.util.Random;

public class Food extends GridObject {

	private Food(int[] pos, Game gameObj) {
		super(pos[0], pos[1], gameObj, Color.RED);
	}

	public Food(Game gameObj){
		this(GenPos(gameObj), gameObj);
	}

	private static int[] GenPos(Game gameObj){
		Random RNG = new Random();
		int x, y;

		while(true){
			x = RNG.nextInt(gameObj.CELL_ROWS);
			y = RNG.nextInt(gameObj.CELL_COLUMNS);

			if(gameObj.getAtPos(x, y) == null){
				return new int[]{x, y};
			}
		}
	}
}
