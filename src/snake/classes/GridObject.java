package snake.classes;

/*
	Authors: Sebastian Cypert
	FileName: GridObject.java
	Description:
		This class is defining a template for objects that go on the snake grid
*/

import javafx.scene.paint.Paint;
import snake.Game;

import java.util.ArrayList;
import java.util.List;

public abstract class GridObject {
	private Paint Fill;
	private final int[] Pos = new int[2];
	private Game GameObj;

	public GridObject(int x, int y, Game gameObj, Paint fill){
		Fill = fill;
		Pos[0] = x;
		Pos[1] = y;
		GameObj = gameObj;

		gameObj.setCell(x, y, this);
	}

	public List<GridObject> getNeighbors(){
		List<GridObject> returnVal = new ArrayList<>();

		for(GridObject i: new GridObject[]{
				GameObj.getAtPos(Pos[0]+1, Pos[1]),
				GameObj.getAtPos(Pos[0]-1, Pos[1]),
				GameObj.getAtPos(Pos[0], Pos[1]+1),
				GameObj.getAtPos(Pos[0], Pos[1]-1)
		}){
			if(i != null){
				returnVal.add(i);
			}
		}

		return returnVal;
	}

	public void Kill(){
		GameObj.clearCell(Pos[0], Pos[1]);
	}

	public Paint getFill(){
		return Fill;
	}

	public int[] getPos(){
		return Pos;
	}

	public Game getGameObj(){
		return GameObj;
	}
}
